<?php
class model 
{
	private $dbConnection;
	
 	public  function __construct(ConnectionInterface $dbConnection) // Используем абстакцию для работы с БД, чтобы не зависить от типа используемой БД.
    {
        $this->dbConnection =  $dbConnection;  
    } 
	
	
	//метод извлечения общего списка целевых объектов (новости, сообщения, товары и т.д.) 
	public function getValueList() 
	{
		try {
			$output_data = $this->dbConnection->getValueList(); //вызов соответствующего метода из класса для работы с БД
			$error = false; 
			$message = "Success"; 
		}
		catch (Throwable $e) {
			$error = true; 
			$message = "Something goes wrong";
			$output_data = "";
			// здесь подробности ошибки можно записать в какой-нибудь лог. 
		}
		finally {
			  $this->dbConnection->closeConnection(); //Закрываем подключение к БД. 
		}
		
		$output = Array("error" => $error, "message" => $message, "data" => $output_data); 
		return $output; //возвращаем результат работы модели в ввиде массива
	}
	
	//метод получения конкретного целевого объекта (например, по его id...) 
	public function getValueItem($data) 
	{
		$itemID = $data['itemId'];
	
		try {
			$output_data = $this->dbConnection->getValueItem($itemID); //вызов соответствующего метода из класса для работы с БД и передача ему идентификатора
			$error = false; 
			$message = "Success"; 
		}
		catch (Throwable $e) {
			$error = true; 
			$message = "Something goes wrong";
			$output_data = "";
			// здесь подробности ошибки можно записать в какой-нибудь лог. 
		}
		finally {
			  $this->dbConnection->closeConnection(); //Закрываем подключение к БД. 
		}
		
		$output = Array("error" => $error, "message" => $message, "data" => $output_data); 
		return $output; //возвращаем результат работы модели в ввиде массива
	}
	
	// ......... Аналогично реализуются все необходимые для реализации API методы модели ......//
	

	
}
?>