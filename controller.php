<?php
/*
 максимально простой контроллер, реализующий три метода API (для работы в формате json)
 Запрос должен содержать данные в формате JSON. Как минимум JSON должен содержать поле 'method'. 
 Также, может содерать поле 'data' с дополнительными данными запроса. 
 В конкретном примере поле 'data' должно содерать вложенный объект с полем itemID. 
 Пример валидного JSON: {"method":"getValueList","data":{"itemID":"12"}}
*/
require "model.php";
header('Content-Type: application/json');

$model = new model($db); //создаем экземляр класса модели. Здесь $db - экземпляр класса, реализующего интерфейс работы с како-либо БД,

// Получение и предварительные проверки запроса в формате json. 
// Для реального приложения кол-во проверок следует увеличить. 

$input = file_get_contents('php://input'); //забираем JSON из тела запроса
$input = json_decode($input);
$method = $input -> method ?? "default";
$data = $input -> data ?? "";  

//Получение необходимых данных из модели и возвращение их клиенту в формате json. 
switch($method)
{
	case "getValueList": 
		$output = $model -> getValueList(); 
		$output = json_encode($output); 
		echo $output; 
	break; 
	case "getValueItem": 
		$output = $model -> getValueItem($data); 
		$output = json_encode($output); 
		echo $output; 
	break; 
	case "setValueItem": 
		$output = $model -> setValueItem($data); 
		$output = json_encode($output); 
		echo $output; 
	break; 
	default: 
		$output = Array("error" => true, "message" => "Wrong method was called");
		$output = json_encode($output);
		echo $output; 		
	break; 	
		
}

?>